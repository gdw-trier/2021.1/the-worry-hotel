using System;
using UnityEngine;
using TMPro;
public static class GlobalVariables
{
    // Volume Settings

    private static float MusicVolume = 0.05f;

    public static event Action<float> OnMusicVolumeChange;

    public static void setMusicVolume(float value)
    {
        MusicVolume = value;
        OnMusicVolumeChange?.Invoke(MusicVolume);
    }

    public static float getMusicVolume()
    {
        return MusicVolume;
    }

    private static float SoundVolume = 0.5f;

    public static event Action<float> OnSoundVolumeChange;

    public static void setSoundVolume(float value)
    {
        SoundVolume = value;
        OnSoundVolumeChange?.Invoke(SoundVolume);
    }

    public static float getSoundVolume()
    {
        return SoundVolume;
    }

    public static long money = 250;

    public static int maxReputation = 100;
    public static int currentReputation = 30;

    public static int maxRooms = 14;
    public static int unlockedRooms = 4;

    public static int guestsHelped = 0;
    public static int guestsLost = 0;

    public static int days = 0;

    public static GameObject winScreen;

    public static GameObject DialogueBox;
    public static TextMeshProUGUI DialogueBoxText;

    public static bool activeDialogue = false;

    // Flag for monster drag:
    public static bool draggingMonster = false;

    // Einstellungen
    public static float clickTime = 0.2f;
    // camera speed
    // dragging speed

    // Time between Dialogue popups per creature in seconds
    public static float TimeBetweenDialogues = 15.0f;

    public static languages currentLanguage = languages.English;

    public enum languages { 
    English,
    German
    };

    public enum states
    {
        Lonely,
        Overwhelmed,
        Anxiety,
        Careless,
        Bored,
        Stressed,
        OverEmotional,
        Numb
    };

    public static void Reset()
    {
        days = 0;
        unlockedRooms = 4;
        money = 500;
        currentReputation = 30;
        guestsHelped = 0;
        guestsLost = 0;
    }
}