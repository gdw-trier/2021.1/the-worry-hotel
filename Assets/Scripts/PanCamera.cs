using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PanCamera : MonoBehaviour
{
    private int monsterlayerMask = 1 << 6;
    //private int roomlayerMask = 1 << 7; 
    private int UIlayerMask = 1 << 5;

    private float pressed;
    private float lastClick;
    private bool startTouch;

    [SerializeField] Transform cameraTransform;
    float cameraSpeed;

    //private float pressed = 0.0f;

    private float backgroundWidth;
    private float backgroundHeight;
    private Vector3 startTouchPosition; 

    //private bool firstpan = true;

    //[SerializeField] float clickTime = 0.1f;
    //private float lastClick;

    //private Vector3 startTouchPosition;

    private Vector2 lastMousePosition;

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;


    private float horizontalEdge;
    private float verticalEdge;
    private Vector2 move;
    private Vector3 boundaries;

    private bool clicked;
    private bool interested = true;

    public event Action<Vector2, Vector3> OnEdgePan;


    public void Start()
    {

#if UNITY_ANDROID
        cameraSpeed = 0.01f;
#endif

#if UNITY_STANDALONE_WIN
        cameraSpeed = 0.01f;
#endif

#if UNITY_EDITOR
        cameraSpeed = 0.01f;
#endif


        Camera cam = GetComponent<Camera>();
        var backgroundsprite = GetComponent<SpriteRenderer>();
        backgroundHeight = backgroundsprite.bounds.size.y;
        backgroundWidth = backgroundsprite.bounds.size.x;
        boundaries = Camera.main.ScreenToWorldPoint(new Vector2(backgroundWidth, backgroundHeight));
        //  Debug.Log(Screen.width + " " +  Screen.height);
        // Debug.Log(Camera.main.pixelWidth + " " + Camera.main.pixelHeight);
        // Debug.Log(Camera.main.aspect);
        // Debug.Log(Camera.main.ScreenToWorldPoint(new Vector2(backgroundWidth, backgroundHeight)));
        // Debug.Log(Camera.main.orthographicSize);

        var vertExtent = Camera.main.orthographicSize;
        var horzExtent = vertExtent * Screen.width / Screen.height;

        // Calculations assume map is position at the origin
        //minX = horzExtent - backgroundWidth / 2.0f;
        //maxX = backgroundWidth / 2.0f - horzExtent;
        minY = vertExtent - backgroundHeight / 2.0f;
        maxY = backgroundHeight / 2.0f - vertExtent;

        horizontalEdge = Screen.width / 10.0f;
        verticalEdge = Screen.height / 10.0f;
        move = new Vector2(0, 0);

    }


    public void OnTouchPress(InputValue value) {
       pressed = value.Get<float>();
    
       if (pressed > 0.0f) {
            if (CheckHitBackground())
            {
                clicked = true;
                if (interested)
                {
                    lastClick = Time.time;
                    startTouch = true;
                    interested = false;
                }
            }
            else
            {
                clicked = false;
                interested = true;
                move = Vector2.zero;
                startTouch = false;
            }
       }
        else
        {
            interested = true;
            clicked = false;
        }

    }

    private void Update()
    {

        //neues movement
        Vector3 cameraPosition = cameraTransform.position;
        //cameraPosition.x += move.x;
        //cameraPosition.x = Mathf.Clamp(cameraPosition.x, minX, maxX);
            cameraPosition.y += move.y;
        cameraPosition.y = Mathf.Clamp(cameraPosition.y, minY, maxY);

        cameraTransform.position = cameraPosition;

        if (!GlobalVariables.draggingMonster)
        {
            //move.x /= 1.5f;
            move.y /= 1.15f;
        }
        else {
            if (move != Vector2.zero) {
                OnEdgePan?.Invoke(move, boundaries);
            }
        }
    }

    public void OnTouchPosition(InputValue value)
    {

        //Drag detect
        lastMousePosition = value.Get<Vector2>();
        if (!GlobalVariables.draggingMonster)
        {
            if (clicked && pressed > 0.0f && Time.time - lastClick > GlobalVariables.clickTime)
            {
                  if (startTouch)
                  {
                      startTouchPosition = Camera.main.ScreenToWorldPoint(lastMousePosition);
                      startTouch = false;
                  }
                  else
                  {
                      Vector3 newPosition = Camera.main.ScreenToWorldPoint(lastMousePosition);
                      //move.x = -(newPosition.x - startTouchPosition.x) * cameraSpeed;
                      move.y = -(newPosition.y - startTouchPosition.y) * cameraSpeed;

                      //altes movement
                      // Vector3 newPosition = Camera.main.ScreenToWorldPoint(lastMousePosition);
                      //Vector3 cameraPosition = cameraTransform.position;
                      //cameraPosition.x -= (newPosition.x - startTouchPosition.x) * cameraSpeed;
                      //cameraPosition.x = Mathf.Clamp(cameraPosition.x, minX, maxX);
                      //cameraPosition.y -= (newPosition.y - startTouchPosition.y) * cameraSpeed;
                      //cameraPosition.y = Mathf.Clamp(cameraPosition.y, minY, maxY);
                      // Camera bounds should work now. 


                      //     }
                  }
            }
        }
        else
        {
            // Check, ob Maus am Bildschirmrand ist:
            move = new Vector2(0, 0);
            //Debug.Log(Screen.width + " " + Screen.height);
            //Debug.Log(lastMousePosition);

            //if (lastMousePosition.x < horizontalEdge)
            //{
            //    move.x -= (horizontalEdge - lastMousePosition.x) * cameraSpeed;
            //}
            //else if (lastMousePosition.x > horizontalEdge * 9.0f)
            //{
            //    move.x += (horizontalEdge - (Screen.width - lastMousePosition.x)) * cameraSpeed;
            //}
            if (lastMousePosition.y < verticalEdge)
            {
                move.y -= (verticalEdge - lastMousePosition.y) * cameraSpeed/10.0f;
            }
            else if (lastMousePosition.y > verticalEdge * 9.0f)
            {
                move.y += (verticalEdge - (Screen.height - lastMousePosition.y)) * cameraSpeed / 10.0f;
            }
        }
    }



    private bool CheckHitBackground()
    {
        Vector3 position = Camera.main.ScreenToWorldPoint(lastMousePosition);
        //Muss Raum/Monster/UI layer Verneinen bevor ok

        RaycastHit2D hit = Physics2D.Raycast(position, Camera.main.transform.forward, 11.0f, (monsterlayerMask| UIlayerMask));
        return hit.collider == null;

    }
}
