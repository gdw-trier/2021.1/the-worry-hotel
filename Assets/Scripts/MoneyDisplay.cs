using TMPro;
using UnityEngine;

public class MoneyDisplay : MonoBehaviour
{
    private TMP_Text textComp;

    void Start()
    {
        textComp = GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    void Update()
    {
        textComp.text = GlobalVariables.money.ToString();
    }
}
