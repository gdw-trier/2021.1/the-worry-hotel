using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class AudioController : MonoBehaviour
{
    // Bellsound
    [SerializeField] AudioClip BellSound;

    // Money falling Sound
    [SerializeField] AudioClip MoneySound;

    // Speechbubblepop Sound
    [SerializeField] AudioClip[] SpeechBubblePops;

    // Room renovation Sound
    [SerializeField] AudioClip RoomRenovation;

    // GameWon Sound
    [SerializeField] AudioClip GameWon;

    [SerializeField] private List<AudioClip> monsterAudio;

    [SerializeField] private List<AudioClip> backgroundMusic;

    [SerializeField] private AudioSource SoundSource;

    [SerializeField] private AudioSource SourceBackgroundMusic;


    // Start is called before the first frame update
    void Start()
    {
        //// Debug.Log(Application.dataPath+ "/Music/Resources/Monstersounds", "*.wav*,);
        //loadAudio("/Music/Resources/Monstersounds", monsterAudio, "Monstersounds/");
        //
        //loadAudio("/Music/Resources", backgroundMusic);
        //Debug.Log(monsterAudio.Count + " " + backgroundMusic.Count);

        SetBackgroundVolume(GlobalVariables.getMusicVolume());

        SetMonsterVolume(GlobalVariables.getSoundVolume());

        GlobalVariables.OnMusicVolumeChange += SetBackgroundVolume;
        GlobalVariables.OnSoundVolumeChange += SetMonsterVolume;

        playBackgroundMusicLooped();
        
    }

//    private void loadAudio(string path, List<AudioClip> clips, string pathAfterResources = "") {
//        string[] MonsterSoundFileNames = Directory.GetFiles(Application.dataPath + path, "*.wav");
//        foreach (string MonsterSoundFileName in MonsterSoundFileNames)
//        {
//            //Debug.Log(MonsterSoundFileName);
//            clips.Add(Resources.Load(pathAfterResources+ Path.GetFileNameWithoutExtension(MonsterSoundFileName)) as AudioClip);
//        }
//    }

    public void playBackgroundMusicLooped()
    {
        int clipID = UnityEngine.Random.Range(0, backgroundMusic.Count);
        SourceBackgroundMusic.loop = true;
        SourceBackgroundMusic.clip = backgroundMusic[clipID];
        SourceBackgroundMusic.Play();
    }

    public void playRandomMonsterSound()
    {
        int clipID = UnityEngine.Random.Range(0, monsterAudio.Count);
        SoundSource.PlayOneShot(monsterAudio[clipID]);
    }

    public void SetBackgroundVolume(float volume)
    {
        if (SourceBackgroundMusic != null)
        {
            SourceBackgroundMusic.volume = volume;
        }
    }

    public void SetMonsterVolume(float volume)
    {
        if (SoundSource)
        {
            SoundSource.volume = volume;
        }
    }

    public void PlayBell()
    {
        if(BellSound == null ||SoundSource == null)
        {
            Debug.LogError("FUCK");
        }
        SoundSource.PlayOneShot(BellSound);
    }

    public void PlayMoneyFalling()
    {
        SoundSource.PlayOneShot(MoneySound);
    }

    public void PlaySpeechBubblePop()
    {
        SoundSource.PlayOneShot(SpeechBubblePops[UnityEngine.Random.Range(0, SpeechBubblePops.Length)]);
    }

    public void PlayRoomRenovated()
    {
        SoundSource.PlayOneShot(RoomRenovation);
    }

    public void PlayGameWon()
    {
        SoundSource.PlayOneShot(GameWon);
    }
}
