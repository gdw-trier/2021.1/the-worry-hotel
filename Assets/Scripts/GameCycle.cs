using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCycle : MonoBehaviour
{

    // Zeit eines Zyklus/updates in Sekunden:
    [SerializeField] private float cycleTime;
    private float lastTick;
    private float currentTick;

    public event Action OnDayChanged;

    // Tagescounter
    [SerializeField] public int dayNumber;

    // Start is called before the first frame update
    void Start()
    {
        lastTick = Time.time;
        currentTick = lastTick;
        dayNumber = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // Update pro Frame - Check ob ein Tag um ist.
        // Alle Berechnungen, welche vom Umschalten abh�ngig sind werden an das Event angeh�ngt.
        currentTick = Time.time;
        if(currentTick - lastTick >= cycleTime)
        {
            lastTick = currentTick;
            OnDayChanged?.Invoke();
            dayNumber++;
        }
        
    }

    // Pause und Resume kommt hier hin
}
