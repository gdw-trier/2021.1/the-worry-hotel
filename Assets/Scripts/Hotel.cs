using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class Hotel : MonoBehaviour
{

    [SerializeField] TextAsset Monsternames;
    private List<string> monsternames;

    // Tageszyklus
    [SerializeField] private GameObject cycleObj;
    private GameCycle cycle;

    // Schwierigkeitsgrad -> Anzahl Tage zwischen neuen Besuchern
    [SerializeField] private int difficulty;

    // Anzahl der Leben des Hotels/Spielers
    [SerializeField] private short lives;

    // Prefab des Monsters
    [SerializeField] GameObject[] monsterPrefabs;
    private WorryEntitiy worry;
    private MonsterInputBehavior behavior;

    // Referenz auf PanCamera skript.
    private PanCamera panCamera;

    // Liste aller aktiven Monster
    [SerializeField] List<GameObject> activeMonsters;

    // Raum, in dem neue Monster spawnen. (-> ? Logic)
    [SerializeField] Room waitingRoom;

    public event Action NewDay;
    private GameObject hotel;

    private AudioController audioController;

    private Dialogue dialogue;

    // Start is called before the first frame update
    void Start()
    {
        monsternames = new List<string>();
        foreach(string name in Monsternames.text.Split('\n')){
            if(!string.IsNullOrWhiteSpace(name))
            {
                monsternames.Add(name);
            }
        }
        if(GlobalVariables.DialogueBox == null)
        {
            GlobalVariables.DialogueBox = GameObject.Find("DialogueBox");
            GlobalVariables.DialogueBoxText = GlobalVariables.DialogueBox.GetComponentInChildren<TextMeshProUGUI>();
        }
        GlobalVariables.DialogueBox.SetActive(false);
        // Get GameCycle from GameCycleObj
        cycle = cycleObj.GetComponent<GameCycle>();
        cycle.OnDayChanged += OnNewDay;

        activeMonsters = new List<GameObject>();

        panCamera = GetComponent<PanCamera>();

        audioController = GetComponentInChildren<AudioController>();

        dialogue = GetComponent<Dialogue>();

        lives = 3;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Event f�r neuen Tag
    void OnNewDay() {
        NewDay?.Invoke();
        
   
        if (cycle.dayNumber % (15 - difficulty) == 0) {
            // Pr�fen, ob im Warteraum noch Platz ist.
            if (waitingRoom.roomAvailable())
            {

                // Erstelle neues Monster im Warteraum aus prefabs
                int spawnprefab = UnityEngine.Random.Range(0, monsterPrefabs.Length);
                GameObject spawn = Instantiate(monsterPrefabs[spawnprefab], new Vector3(0, 0, +100), transform.rotation);
                spawn.transform.localScale = new Vector3(0.2f, 0.2f);
                spawn.layer = 6;

                // Zuf�lliger name aus Liste

                spawn.name = randomName();

                // Anmelden des Hotels an das Event 'Tod' des Monsters
                worry = spawn.GetComponent<WorryEntitiy>();
                worry.OnHealthZero += OnHealthZero;

                // �bergeben des Raumes an das Monster
                worry.UpdateRoom(waitingRoom);
                worry.SetDialogue(dialogue);
                waitingRoom.addGuest(spawn);

                // Anmelden des Monsters an die Bewegung der Kamera
                behavior = spawn.GetComponent<MonsterInputBehavior>();
                behavior.SetCamera(panCamera);
                behavior.SetAudioController(audioController);

                audioController.PlayBell();

                //activeMonsters.Add(spawn);
            }
            // Kein freier Platz im Warteraum -> Lebenabzug?
            else
            {

            }
        
        }
    }

    // Event f�r 'Tod' eines Monsters
    void OnHealthZero(GameObject monster)
    {
        // do stuff

        lives--;

        // delete monster
        Debug.Log("ZERO");
        MonsterInputBehavior monsterinput = monster.GetComponentInChildren<MonsterInputBehavior>();
        monsterinput.Exit();
        GlobalVariables.guestsLost++;
    }

    private string randomName()
    {
        return monsternames[UnityEngine.Random.Range(0, monsternames.Count)];
    }
}
