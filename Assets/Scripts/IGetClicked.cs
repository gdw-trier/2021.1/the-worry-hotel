using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class IGetClicked : MonoBehaviour
{
    private float pressedValue;

    public void OnTouchPress(InputValue value) {
        pressedValue = value.Get<float>();

        
    }

    public void OnTouchPosition(InputValue value) {
        if (pressedValue > 0.0f)
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(value.Get<Vector2>());
            RaycastHit2D hit = Physics2D.Raycast(position, Camera.main.transform.forward, 11.0f);
            if(hit.collider != null) { 
                Debug.Log("Raycast hit something");
                // check
                if (hit.collider.gameObject == gameObject)
                {
                    Debug.Log(gameObject.name + " was clicked");
                }
            }
        }
    }
}
