using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReputationManipulation : MonoBehaviour
{
    [SerializeField]
    private Slider slider;


    private void Update()
    {
        if(GlobalVariables.currentReputation < 0)
        {
            GlobalVariables.currentReputation = 0;
        }

        if (GlobalVariables.currentReputation > 100)
        {
            GlobalVariables.currentReputation = 100;
        }

        slider.value = GlobalVariables.currentReputation;
    }
   
}
