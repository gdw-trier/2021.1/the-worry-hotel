using UnityEngine;
using DataAccess;
using System.IO;
using System.Globalization;

public class Dialogue : MonoBehaviour
{
    [SerializeField] private TextAsset[] DialogueFiles;

   public struct Translation
    {
        public string english { get; set; }
        public string german { get; set; }

        public string getText(GlobalVariables.languages language)
        {
            switch (language)
            {
                case GlobalVariables.languages.English:
                    return english;
                case GlobalVariables.languages.German:
                    return german;
            }
            return string.Empty;
        }
    }

    Translation[] Lonely;
    Translation[] Careless;
    Translation[] Boredom;
    Translation[] Anxiety;
    Translation[] Numb;
    Translation[] OverlyEmotional;
    Translation[] Stressed;
    Translation[] Overwhelmed;


    // Start is called before the first frame update
    void Start()
    {
        Lonely = loadDialogue(0, Lonely);
        Careless = loadDialogue(1, Careless);
        Boredom = loadDialogue(2, Boredom);
        Anxiety = loadDialogue(3, Anxiety);
        Numb = loadDialogue(4, Numb);
        OverlyEmotional = loadDialogue(5, OverlyEmotional);
        Stressed = loadDialogue(6, Stressed);
        Overwhelmed = loadDialogue(7, Overwhelmed);
    }

    private Translation[] loadDialogue(int ID, Translation[] array)
    {
        MutableDataTable dt = DataTable.New.ReadFromString(DialogueFiles[ID].text);
        array = new Translation[dt.NumRows];
        for (int i = 0; i < dt.NumRows; i++)
        {
            Row x = dt.GetRow(i);
            Translation a = x.As<Translation>();
            array[i] = a;
        }
        return array;
    }

    public string randomDialogue(GlobalVariables.languages language, GlobalVariables.states state)
    {
        switch (state)
        {
            case GlobalVariables.states.Lonely:
                return Lonely[UnityEngine.Random.Range(0, Lonely.Length)].getText(language);
            case GlobalVariables.states.Overwhelmed:
                return Overwhelmed[UnityEngine.Random.Range(0, Overwhelmed.Length)].getText(language);
            case GlobalVariables.states.Careless:
                return Careless[UnityEngine.Random.Range(0, Careless.Length)].getText(language);
            case GlobalVariables.states.Bored:
                return Boredom[UnityEngine.Random.Range(0, Boredom.Length)].getText(language);
            case GlobalVariables.states.Anxiety:
                return Anxiety[UnityEngine.Random.Range(0, Anxiety.Length)].getText(language);
            case GlobalVariables.states.Numb:
                return Numb[UnityEngine.Random.Range(0, Numb.Length)].getText(language);
            case GlobalVariables.states.OverEmotional:
                return OverlyEmotional[UnityEngine.Random.Range(0, OverlyEmotional.Length)].getText(language);
            case GlobalVariables.states.Stressed:
                return Stressed[UnityEngine.Random.Range(0, Stressed.Length)].getText(language);
        }
        return string.Empty;
    }
   
}
