using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    void OnDisable()
    {
        Time.timeScale = 1f;
    }

    void OnEnable()
    {
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        gameObject.SetActive(false);
    }

    public void Leave()
    {
        GlobalVariables.Reset();
        gameObject.SetActive(false);
        SceneManager.LoadScene("MainMenu");
    }

}
