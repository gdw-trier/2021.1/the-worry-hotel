using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndGameStatistic : MonoBehaviour
{
    [SerializeField] private TMP_Text CONGRATS;
    [SerializeField] private TMP_Text STATISTIC;
    

    void OnEnable()
    {
        if (GlobalVariables.currentLanguage == GlobalVariables.languages.English)
        {
            STATISTIC.text = "You received...\n" + GlobalVariables.guestsHelped + " positive reviews\n" + GlobalVariables.guestsLost + " negative reviews";
            CONGRATS.text = "The Hotel is finished, I can finally leave. Aren't you excited?";
        } 
        else
        {
            STATISTIC.text = "Das Hotel erhielt...\n" + GlobalVariables.guestsHelped + " positive Bewertungen\n" + GlobalVariables.guestsLost + " negative Bewertungen";
            CONGRATS.text = "Das Hotel ist ausgebaut, ich kann endlich verschwinden. Da staunen Sie!";
        }
    }
}
