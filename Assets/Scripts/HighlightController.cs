using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightController : MonoBehaviour
{

    [SerializeField] GameObject[] Highlighters;

    private int current = 0;

    public void SetStartEnable(int ID)
    {
        if(ID >= 0 && ID < Highlighters.Length)
        {
            for(int i = 0; i < Highlighters.Length; i++)
            {
                if(i == ID)
                {
                    Highlighters[i].SetActive(true);
                }
                else
                {
                    Highlighters[i].SetActive(false);
                }
            }
        }

    }

    public void changeHighlight(int ID)
    {
        if (current != ID && ID >= 0 && ID < Highlighters.Length)
        {
            Highlighters[current].SetActive(false);
            //Debug.Log("Disabled " + current + " Enabled " + ID);
            current = ID;
            Highlighters[current].SetActive(true);
        }
    }

}
