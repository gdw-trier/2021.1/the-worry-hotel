using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudioController : MonoBehaviour
{

    [SerializeField] AudioSource MusicSource;

    [SerializeField] AudioSource SoundSource;

    [SerializeField] AudioClip[] ControlSounds;

    // Start is called before the first frame update
    void Start()
    {

        SetMusicVolume(GlobalVariables.getMusicVolume());
        SetSoundVolume(GlobalVariables.getSoundVolume());

        GlobalVariables.OnSoundVolumeChange += SetSoundVolume;
        GlobalVariables.OnMusicVolumeChange += SetMusicVolume;
    }

    public void playRandomSound()
    {
        SoundSource.PlayOneShot(ControlSounds[UnityEngine.Random.Range(0, ControlSounds.Length)]);
    }

    public void SetSoundVolume(float volume)
    {
        if (SoundSource != null)
        {
            SoundSource.volume = volume;
        }
    }

    public void SetMusicVolume(float volume)
    {
        if(MusicSource != null)
        {
            //MusicSource.gameObject.GetComponentInChildren<AudioSource>();
            MusicSource.volume = volume;
        }
    }
}
