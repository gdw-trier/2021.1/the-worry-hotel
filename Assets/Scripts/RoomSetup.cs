using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSetup : MonoBehaviour
{
    private GameObject[] rooms;

    private List<GameObject> singles = new List<GameObject>();
    private List<GameObject> twins = new List<GameObject>();
    private List<GameObject> triples = new List<GameObject>();
    private List<GameObject> quadrupels = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        rooms = GameObject.FindGameObjectsWithTag("Room");        

        //sortRoomsByCapacity();
        //assignRandomRoomEffects();
    }

    // sort rooms by capacity of guests
    void sortRoomsByCapacity()
    {
        foreach (GameObject room in rooms)
        {
            switch (room.GetComponent<Room>().getCapacity())
            {
                case 1:
                    singles.Add(room);
                    break;
                case 2:
                    twins.Add(room);
                    break;
                case 3:
                    triples.Add(room);
                    break;
                case 4:
                    quadrupels.Add(room);
                    break;
            }
        }
    }

    // assign effects for the rooms
    void assignRandomRoomEffects()
    {
        assignSingleEffects();
        assignTwinEffects();
    }

    void assignSingleEffects()
    {
        //randomize rooms for assignment of effects
        singles = randomizeRooms(singles);
       
        for (int i = 0; i < singles.Count; i+=2)
        {
            int j = i / 2;
            switch(j)
            {
                case 0: //assign positive/negative effects for "activity"
                    singles[i].GetComponent<Room>().setEffects(1, 0, 0);
                    singles[i+1].GetComponent<Room>().setEffects(-1, 0, 0);
                    break;
                case 1: //assign positive/negative effects for "awareness"
                    singles[i].GetComponent<Room>().setEffects(0, 1, 0);
                    singles[i + 1].GetComponent<Room>().setEffects(0, -1, 0);
                    break;
                case 2: //assign positive/negative effects for "emotion"
                    singles[i].GetComponent<Room>().setEffects(0, 0, 1);
                    singles[i + 1].GetComponent<Room>().setEffects(0, 0, -1);
                    break;
            }
        }
    }

    void assignTwinEffects()
    {
        //randomize rooms for assignment of effects
        twins = randomizeRooms(twins);

        // int j = 0;

        for (int i = 0; i < twins.Count; ++i)
        {
           //  twins[i].GetComponent<Room>().setEffects();
        }
    }

    List<GameObject> randomizeRooms(List<GameObject> rooms)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            GameObject temp = rooms[i];
            int randomIndex = Random.Range(i, rooms.Count);
            rooms[i] = rooms[randomIndex];
            rooms[randomIndex] = temp;
        }

        return rooms;
    }
}
