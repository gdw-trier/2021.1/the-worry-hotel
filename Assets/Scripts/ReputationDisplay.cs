using UnityEngine;
using UnityEngine.UI;

public class ReputationDisplay : MonoBehaviour
{
    [SerializeField] private Slider slider;

    public void Update()
    {
        slider.value = GlobalVariables.currentReputation;
    }
}
