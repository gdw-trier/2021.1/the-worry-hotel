using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManagementScript : MonoBehaviour
{
    [SerializeField] private GameObject optionsPanel;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider soundSlider;

    [SerializeField] private Image english;
    [SerializeField] private Image deutsch;

    // Start is called before the first frame update
    void Start()
    {
        musicSlider.value = GlobalVariables.getMusicVolume();
        soundSlider.value = GlobalVariables.getSoundVolume();
        optionsPanel.SetActive(false);  
    }

    public void showOptions()
    {
        optionsPanel.SetActive(true);
    }

    public void hideOptions()
    {
        optionsPanel.SetActive(false);
    }


    //Language
    public void setEnglishLanguage()
    {
        GlobalVariables.currentLanguage = GlobalVariables.languages.English;
        english.color = new Color(english.color.r, english.color.g, english.color.b, 1.0f);
        deutsch.color = new Color(deutsch.color.r, deutsch.color.g, deutsch.color.b, 0.45f);
    }

    public void setDeutscheSprache()
    {
        GlobalVariables.currentLanguage = GlobalVariables.languages.German;
        english.color = new Color(english.color.r, english.color.g, english.color.b, 0.45f);
        deutsch.color = new Color(deutsch.color.r, deutsch.color.g, deutsch.color.b, 100f);
    }



    //Volume

    public void setMusicVolume(Slider music)
    {
        GlobalVariables.setMusicVolume(music.value);
    }

    public void setSoundVolume(Slider sound)
    {
        GlobalVariables.setSoundVolume(sound.value);
    }
}
