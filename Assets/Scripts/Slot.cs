using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    private bool OCCUPIED = false;

    public bool isEmpty()
    {
        return !OCCUPIED;
    }

    public void addGuest(GameObject guest)
    {
        OCCUPIED = true;
        guest.transform.parent = gameObject.transform;
        guest.transform.localPosition = new Vector3(0.0f, -0.28f, 0);
    }

    public void removeGuest(GameObject guest)
    {
        OCCUPIED = false;
        guest.transform.SetParent(null);
    }
}
