using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

public class MonsterInputBehavior : MonoBehaviour
{
    private float pressedValue;

    private bool clicked;

    private string dialogueText;

    private Vector3 lastPosition;
    private Vector2 lastMousePosition;

    private float lastClick;

    [SerializeField] GameObject DialogueIcon;

    [SerializeField] GameObject DialogueArrow;

    [SerializeField] GameObject StatusUI;

    [SerializeField] GameObject DialogUI;

    bool Onexit;

    [SerializeField] AudioController audioController;

    [SerializeField] PanCamera panCamera;

    [SerializeField] WorryEntitiy worry;

    private void Start()
    {

        StatusUI.SetActive(false);
        DialogueArrow.SetActive(false);
        DialogueIcon.SetActive(false);
        Onexit = false;
        if(panCamera != null)
        {
            panCamera.OnEdgePan += OnEdgePanFix;
        }
    }

    public void SetCamera(PanCamera panCamera)
    {
        if(this.panCamera != null)
        {
            this.panCamera.OnEdgePan -= OnEdgePanFix;
        }
        this.panCamera = panCamera;
        this.panCamera.OnEdgePan += OnEdgePanFix;
    }

    public void SetAudioController(AudioController audioController)
    {
        this.audioController = audioController;
    }

    public void OnTouchPress(InputValue value)
    {
        pressedValue = value.Get<float>();
        //Debug.Log(pressedValue + " " +  Time.time +" " + lastClick);
        if (pressedValue > 0.0f)
        {
            lastClick = Time.time;
            lastPosition = gameObject.transform.position;
            if (CheckClickOnGameObject())
            {
                clicked = true;
            }
            else if(CheckClickOnDialogueBoxIcon())
            {
                if (GlobalVariables.activeDialogue == false)
                {
                    worry.DialoguePop();
                    StartCoroutine(ShowDialogue());
                    StatusUI.SetActive(false);
                }
            }
            else
            {
                StatusUI.SetActive(false);
                DialogUI.SetActive(true);
            }
        }
        else if (pressedValue == 0.0f && Time.time - lastClick < GlobalVariables.clickTime)
        {
            // Click -> Statistik zeigen
            if (CheckClickOnGameObject())
            {
                StatusUI.SetActive(true);
                DialogUI.SetActive(false);
                audioController.playRandomMonsterSound();
            }
            clicked = false;
        }

        //Drag done -> Check, ob wir �ber einem Raum losgelassen haben
        else if (clicked && pressedValue == 0.0f && Time.time - lastClick >= GlobalVariables.clickTime)
        {
            GlobalVariables.draggingMonster = false;
            clicked = false;
            StatusUI.SetActive(false);
            //Debug.Log("Drag done");
            Room hitRoom = CheckRoomHit();
            
            if (hitRoom != null && !worry.checkRoom(hitRoom) && hitRoom.roomAvailable())
            {
                //Debug.Log("Found room");
                worry.UpdateRoom(hitRoom);
                hitRoom.addGuest(gameObject);
                audioController.playRandomMonsterSound();
            }
            else if(!Onexit) {
                gameObject.transform.position = lastPosition;
            }

        }
        

    }

    public void OnTouchPosition(InputValue value)
    {
        lastMousePosition = value.Get<Vector2>();
        //Dragging
        if (clicked && pressedValue > 0.0f && Time.time - lastClick >= GlobalVariables.clickTime) {
            GlobalVariables.draggingMonster = true;
            Vector3 position = Camera.main.ScreenToWorldPoint(lastMousePosition);
            position.z = 0;
            gameObject.transform.position = position;
            StatusUI.SetActive(false);

        }
    }

    public void OnEdgePanFix(Vector2 move, Vector3 boundaries){
        if (clicked)
        {
            Vector3 position = gameObject.transform.position;
            position.x += move.x;
            position.x = Mathf.Clamp(position.x, boundaries.x, -boundaries.x);
            position.y += move.y;
            position.y = Mathf.Clamp(position.y, boundaries.y, -boundaries.y);
            gameObject.transform.position = position;
        }
    }


    private bool CheckClickOnGameObject() {
        Vector3 position;
        RaycastHit2D hit;
        position = Camera.main.ScreenToWorldPoint(lastMousePosition);
        position.z = -10.0f;
        hit = Physics2D.Raycast(position, Camera.main.transform.forward, 11.0f, LayerMask.GetMask("Monsters"));   
        if (hit.collider != null)
        {
          return hit.collider.gameObject == gameObject;
        }
        return false;
    }

    private bool CheckClickOnDialogueBoxIcon()
    {
        Vector3 position;
        RaycastHit2D hit;
        position = Camera.main.ScreenToWorldPoint(lastMousePosition);
        position.z = -10.0f;
        hit = Physics2D.Raycast(position, Camera.main.transform.forward, 11.0f, LayerMask.GetMask("Dialogue"));
        if (hit.collider != null)
        {
            return hit.collider.gameObject == DialogueIcon.gameObject;
        }
        return false;
    }

    private Room CheckRoomHit()
    {
        Vector3 position;
        RaycastHit2D hit;
        position = Camera.main.ScreenToWorldPoint(lastMousePosition);
        position.z = -10.0f;
        hit = Physics2D.Raycast(position, Camera.main.transform.forward, 11.0f, LayerMask.GetMask("Rooms"));
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.TryGetComponent<Exit>(out Exit exit))
            {
                worry.UpdateRoom(exit);
                exit.addGuest(gameObject);
                Exit();
                return null;
            }
            return hit.collider.gameObject.GetComponent<Room>();
        }
        return null;
        
    }

    public void Exit()
    {
        GlobalVariables.guestsHelped++;
        if (worry.ExitMoney())
        {
            audioController.PlayMoneyFalling();
        }
        //audioController.playRandomMonsterSound();
        worry.CheckOut();
        Destroy(gameObject, 0.5f);
        Onexit = true;
    }


    public void ShowDialogueIcon(string text)
    {
        dialogueText = text;
        DialogueIcon.SetActive(true);
    }

    IEnumerator ShowDialogue()
    {
        audioController.PlaySpeechBubblePop();

        DialogueIcon.SetActive(false);
        DialogueArrow.SetActive(true);

        GlobalVariables.activeDialogue = true;
        Vector3 pos = GlobalVariables.DialogueBox.transform.position;
        pos.z = 0;
        pos.x = 0;
        pos.y = worry.RoomHeight() - 3.5f;
        GlobalVariables.DialogueBox.transform.position = pos;


        if (GlobalVariables.DialogueBoxText == null)
        {
            GlobalVariables.DialogueBoxText = GlobalVariables.DialogueBox.GetComponentInChildren<TextMeshProUGUI>();
        }
        //Debug.Log(GlobalVariables.DialogueBoxText != null);
        GlobalVariables.DialogueBoxText.text = dialogueText;
        GlobalVariables.DialogueBox.SetActive(true);
        yield return new WaitForSeconds(2);
        DialogueArrow.SetActive(false);
        GlobalVariables.DialogueBox.SetActive(false);
        GlobalVariables.activeDialogue = false;
    }
}


