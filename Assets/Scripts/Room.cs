using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Room : MonoBehaviour
{
    private int[] UNLOCK_PRICES = new int[] { 100, 200, 300, 330 };
    private int[] UPDATE_PRICES = new int[] { 80, 150, 200, 280 };

    // room is unlocked
    [SerializeField] private bool ACTIVE = false;
    private bool UPDATED = false;

    private int unlockPrice;
    private int updatePrice;

    // capacity of possible guests 
    [SerializeField][Range(1, 4)] private int CAPACITY = 1;
    private int currentGuests = 0;
    [SerializeField] private GameObject[] SLOTS;

    // can either be "-2", "-1", "0" or "1", "2"
    [SerializeField] private int EFFECT_ACTIVITY = 0;
    [SerializeField] private int EFFECT_AWARENESS = 0;
    [SerializeField] private int EFFECT_EMOTION = 0;
    private int EFFECT_SOCIAL = -1;

    // apply room effects
    public event Action<int, int, int, int> RoomEffect;

    // Start is called before the first frame update
    void Start()
    {
        if(this.CAPACITY == 1)
        {
            EFFECT_SOCIAL -= 1;
        }

        assignEffects();
 
        unlockPrice = UNLOCK_PRICES[CAPACITY - 1];
        updatePrice = UPDATE_PRICES[CAPACITY - 1];

        Hotel hotel = GetComponentInParent<Hotel>();
        hotel.NewDay += onNewDay;
    }

    void assignEffects()
    {
        int slot = 0;
        Sprite effectSprite;
        int[] effectValues = new int[] { EFFECT_EMOTION, EFFECT_AWARENESS, EFFECT_ACTIVITY };
        string[] effects = new string[] { "Emotion", "Awareness", "Activity" };

        for (int i = 0; i < effectValues.Length; ++i)
        {
            if(effectValues[i] != 0)
            {
                if (effectValues[i] > 0)
                    effectSprite = Resources.Load<Sprite>("" + effects[i] + "_Plus");
                else 
                    effectSprite = Resources.Load<Sprite>("" + effects[i] + "_Minus");

               //Debug.Log(gameObject.name + " " + effects[i] + " " + effectValues[i]);

                SLOTS[slot].GetComponentInChildren<Image>().sprite = effectSprite;
                ++slot;
            }
        }
    }

    public void setEffects(int emotion, int awareness, int activity)
    {
        this.EFFECT_ACTIVITY = activity;
        this.EFFECT_AWARENESS = awareness;
        this.EFFECT_EMOTION = emotion;
    }

    public void onNewDay()
    {
        RoomEffect?.Invoke(EFFECT_EMOTION, EFFECT_AWARENESS, EFFECT_ACTIVITY, EFFECT_SOCIAL);
    }

    public void unlockRoom()
    {
        this.ACTIVE = true;
    }

    /*public void updateRoom()
    {
        //TODO: read Message from file
        if (Money.money - updatePrice < 0)
            // TODO: Show Dialog
            Debug.Log("You can't afford this yet. Release patients to gain \"x\".");
        else
        {
            Money.money -= updatePrice;
            this.UPDATED = true;

            EFFECT_ACTIVITY *= 2;
            EFFECT_AWARENESS *= 2;
            EFFECT_EMOTION *= 2;

            // TODO: Change Sprite
        }
    } */

    public bool addGuest(GameObject guest)
    {
        if (currentGuests + 1 <= CAPACITY)
        {
            // Find free slot
            for (int i = 0; i < SLOTS.Length; ++i) 
            {
                if(SLOTS[i].GetComponent<Slot>().isEmpty())
                {
                    SLOTS[i].GetComponent<Slot>().addGuest(guest);
                    currentGuests++;
                    this.EFFECT_SOCIAL += 1;
                    break;
                }
            }
            return true;
        }
        else
            return false;
    }

    public bool roomAvailable()
    {
        return ACTIVE && (currentGuests + 1 <= CAPACITY);
    }

    public void removeGuest(GameObject guest)
    {
        for (int i = 0; i < SLOTS.Length; ++i)
        {
            if (guest.transform.parent == SLOTS[i].transform)
            {
                SLOTS[i].GetComponent<Slot>().removeGuest(guest);
                currentGuests--;
                this.EFFECT_SOCIAL -= 1;
            }
        }
    }

    public int getCapacity()
    {
        return this.CAPACITY;
    }

    public int getUnlockPrice()
    {
        return this.unlockPrice;
    }
}
