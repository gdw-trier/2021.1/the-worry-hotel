using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockRoom : MonoBehaviour
{ 
    [SerializeField] private GameObject ROOM;
    [SerializeField] private GameObject ERRORMESSAGE;
    [SerializeField] private GameObject PRICETAG;

    private AudioController audioController;

    public void Start()
    {
        audioController = GameObject.Find("audioSystem").GetComponent<AudioController>();
        if (GlobalVariables.winScreen == null)
        {
            GlobalVariables.winScreen = GameObject.Find("GameWon");
            
        }
        GlobalVariables.winScreen.SetActive(false);
    }

    public void unlockRoom()
    {
        int unlockPrice = ROOM.GetComponent<Room>().getUnlockPrice();

        if (GlobalVariables.money - unlockPrice < 0)
        {
            ERRORMESSAGE.gameObject.SetActive(true);
            PRICETAG.gameObject.SetActive(false);
            StartCoroutine(hideError());
        }
        else
        {
            transform.parent.gameObject.SetActive(false);
            GlobalVariables.money -= unlockPrice;
            ROOM.GetComponent<Room>().unlockRoom();
            GlobalVariables.unlockedRooms++;


            // check if game is won
            if (GlobalVariables.unlockedRooms == GlobalVariables.maxRooms)
            {
                GlobalVariables.winScreen.SetActive(true);
                audioController.PlayGameWon();
            }
            else
            {
                audioController.PlayRoomRenovated();
            }
        }
    }

    IEnumerator hideError()
    {
        yield return new WaitForSeconds(2);
        ERRORMESSAGE.gameObject.SetActive(false);
        PRICETAG.gameObject.SetActive(true);
    }
}
