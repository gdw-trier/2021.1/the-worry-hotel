using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartController : MonoBehaviour
{
    [SerializeField] Image[] Hearts;

    private bool[] heartvalues = { false, false, false, false };

    [SerializeField] private Sprite Blank_Herz;

    [SerializeField] private Sprite Activity_Herz;

    [SerializeField] private Sprite Awareness_Herz;

    [SerializeField] private Sprite Emotion_Herz;

    [SerializeField] private Sprite Social_Herz;

    public bool SetAwarenessHerz(bool status)
    {
        if (status)
        {
            Hearts[1].sprite = Awareness_Herz;

        }
        else
        {
            Hearts[1].sprite = Blank_Herz;
        }
        if (heartvalues[1] != status)
        {
            heartvalues[1] = status;
            return true;
        }
        return false;
    }

    public bool SetActivityHerz(bool status)
    {
        if (status)
        {
            Hearts[2].sprite = Activity_Herz;
        }
        else
        {
            Hearts[2].sprite = Blank_Herz;
        }
        if (heartvalues[2] != status)
        {
            heartvalues[2] = status;
            return true;
        }
        return false;
    }

    public bool SetEmotionHerz(bool status)
    {
        if (status)
        {
            Hearts[0].sprite = Emotion_Herz;
        }
        else
        {
            Hearts[0].sprite = Blank_Herz;
        }
        if (heartvalues[0] != status)
        {
            heartvalues[0] = status;
            return true;
        }
        return false;
    }

    public bool SetSocialHerz(bool status)
    {
        if (status)
        {
            Hearts[3].sprite = Social_Herz;
        }
        else
        {
            Hearts[3].sprite = Blank_Herz;
        }
        if (heartvalues[3] != status)
        {
            heartvalues[3] = status;
            return true;
        }
        return false;
    }
}
