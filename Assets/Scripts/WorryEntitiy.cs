using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WorryEntitiy : MonoBehaviour
{

    private float lastDialoguePop;

    [SerializeField] private TextMeshProUGUI nametag;

    [SerializeField] private Dialogue dialogue;

    [SerializeField] MonsterInputBehavior inputBehavior;

    [SerializeField] private HighlightController[] highlightControllers;
    [SerializeField] private HeartController heartController;

    private int numHearts = 0;

    // der raum, in dem das Monster ist
    [SerializeField] private Room room;

    [SerializeField] private Animator animator;
    // Jedes WorryEntity (monster) h�rt auf den Raum in dem es ist.
    // [SerializeField] private Room room;

    // Event, falls Health = 0;
    public event Action<GameObject> OnHealthZero;


    // Pers�nlichkeit, ausgelagert f�r Lesbarkeit
    private Personalities.personalities personality;

    // Monster Versuchen nur alle talkative Tage mit dem Spieler zu reden.
    private int talkative = 3;
    private int lastTalked = 0;

    // Mental Values
    private int[] MentalValues = new int[4];
    [SerializeField] private int Emotion
    {
        get => MentalValues[0];
        set => MentalValues[0] = value;
    }
    
    [SerializeField]
    private int Awareness
    {
        get => MentalValues[1];
        set => MentalValues[1] = value;
    }

    [SerializeField]
    private
    int Activity
    {
        get => MentalValues[2];
        set => MentalValues[2] = value;
    }
    [SerializeField]
    private
    int Social
    {
        get => MentalValues[3];
        set => MentalValues[3] = value;
    }


    // health = Social + Activity + Awareness + Emotion / 4.0f;
    private float Health;

    // Start is called before the first frame update
    void Start()
    {

        lastDialoguePop = Time.time;

        nametag.text = gameObject.name;

        room.RoomEffect += OnRoomUpdate;
        // Random Startwerte bzw Pers�nlichkeit
        //var list = GetComponents(typeof(Component));
        //for (int i = 0; i < list.Length; i++)
        //{
        //    Debug.Log(list[i].name + list[i].ToString());
        //}

        // Randomized Starting Values
        for(int i = 0; i<4; i++)
        {
            if(UnityEngine.Random.Range(0.0f, 1.0f) > 0.5f)
            {
                MentalValues[i] = UnityEngine.Random.Range(70, 100);   
            }
            else
            {
                MentalValues[i] = UnityEngine.Random.Range(0, 30);
            }
        }

        //Debug.Log(gameObject.name + " - Emotion: " + Emotion + " Awareness: " + Awareness + " Activity: " + Activity + " Social: " + Social);

        highlightControllers[0].SetStartEnable(Emotion / 20);
        highlightControllers[1].SetStartEnable(Awareness / 20);
        highlightControllers[2].SetStartEnable(Activity / 20);
        highlightControllers[3].SetStartEnable(Social / 20);
    }

    public void UpdateRoom(Room roomNew)
    {
        CheckOut();
        room = roomNew;
        room.RoomEffect += OnRoomUpdate;
    }

    public float RoomHeight()
    {
        return room.transform.position.y;
    }

    public void CheckOut()
    {
        if (room != null)
        {
            room.RoomEffect -= OnRoomUpdate;
            room.removeGuest(gameObject);
        }
    }

    public void SetDialogue(Dialogue dialogue)
    {
        this.dialogue = dialogue;
    }

    public bool checkRoom(Room roomNew)
    {
        return room == roomNew;
    }

    // Feuert jeden Tag einmalig
    public void OnRoomUpdate(int emotion, int awareness, int activity, int social)
    {
        lastTalked++;
        var weights = Personalities.getPersonaliyWeights(personality);
        Emotion += emotion;
        Emotion = Emotion < 0 ? 0 : Emotion;
        Emotion = Emotion > 100 ? 100 : Emotion;

        Awareness += awareness;
        Awareness = Awareness < 0 ? 0 : Awareness;
        Awareness = Awareness > 100 ? 100 : Awareness;

        Activity += activity;
        Activity = Activity < 0 ? 0 : Activity;
        Activity = Activity > 100 ? 100 : Activity;

        Social += social;
        Social = Social < 0 ? 0 : Social;
        Social = Social > 100 ? 100 : Social;

        if (lastTalked > talkative && (Time.time - lastDialoguePop) > GlobalVariables.TimeBetweenDialogues)
        {
            if (Emotion > 80)
            {
                inputBehavior.ShowDialogueIcon(dialogue.randomDialogue(GlobalVariables.currentLanguage, GlobalVariables.states.OverEmotional));
            }
            else if (Emotion < 20)
            {
                inputBehavior.ShowDialogueIcon(dialogue.randomDialogue(GlobalVariables.currentLanguage, GlobalVariables.states.Numb));
            }


            if (Awareness > 80)
            {
                inputBehavior.ShowDialogueIcon(dialogue.randomDialogue(GlobalVariables.currentLanguage, GlobalVariables.states.Anxiety));
            }
            else if (Awareness < 20)
            {
                inputBehavior.ShowDialogueIcon(dialogue.randomDialogue(GlobalVariables.currentLanguage, GlobalVariables.states.Careless));
            }


            if (Activity > 80)
            {
                inputBehavior.ShowDialogueIcon(dialogue.randomDialogue(GlobalVariables.currentLanguage, GlobalVariables.states.Stressed));
            }
            else if (Activity < 20)
            {
                inputBehavior.ShowDialogueIcon(dialogue.randomDialogue(GlobalVariables.currentLanguage, GlobalVariables.states.Bored));
            }


            if (Social > 80)
            {
                inputBehavior.ShowDialogueIcon(dialogue.randomDialogue(GlobalVariables.currentLanguage, GlobalVariables.states.Overwhelmed));
            }
            else if (Social < 20)
            {
                inputBehavior.ShowDialogueIcon(dialogue.randomDialogue(GlobalVariables.currentLanguage, GlobalVariables.states.Lonely));
            }
        }

        highlightControllers[0].changeHighlight(Emotion / 20);
        if (Emotion > 40 && Emotion < 60)
        {
           if(heartController.SetEmotionHerz(true))
            {
                numHearts++;
            }

        }
        else
        {
            if (heartController.SetEmotionHerz(false)) {
                numHearts--;
            }
 
        }


        highlightControllers[1].changeHighlight(Awareness / 20);
        if (Awareness > 40 && Awareness < 60)
        {
            if (heartController.SetAwarenessHerz(true))
            {
                numHearts++;
            }
        }
        else
        {
            if (heartController.SetAwarenessHerz(false))
            {
                numHearts--;
            }
        }


        highlightControllers[2].changeHighlight(Activity / 20);
        if (Activity > 40 && Activity < 60)
        {
            if (heartController.SetActivityHerz(true))
            {
                numHearts++;
            }
        }
        else
        {
            if (heartController.SetActivityHerz(false))
            {
                numHearts--;
            }
        }


        highlightControllers[3].changeHighlight(Social / 20);


        
        if (Social > 40 && Social < 60)
        {
            if (heartController.SetSocialHerz(true))
            {
                numHearts++;
            }
        }
        else
        {
            if (heartController.SetSocialHerz(false))
            {
                numHearts--;
            }
        }


        Health = (Social + Activity + Awareness + Emotion) / 4.0f;
        if (animator != null)
        {
            if (numHearts < 3)
                animator.SetBool("Happy", false);
            else
                animator.SetBool("Happy", true);
        }

        if(Health <= 0.1f)
        {
            OnHealthZero?.Invoke(transform.parent.gameObject);
        }
    }

        

    public bool ExitMoney()
    {
        int sweetspot = 0;
        int okspot = 0;
        int badspot = 0;

        switch(Emotion / 20)
        {
            case 0:
            case 4:
            case 5: badspot++; break;
            case 1:
            case 3: okspot++; break;
            case 2: sweetspot++; break;
        }
        switch (Awareness / 20)
        {
            case 0:
            case 4:
            case 5: badspot++; break;
            case 1:
            case 3: okspot++; break;
            case 2: sweetspot++; break;
        }
        switch (Activity / 20)
        {
            case 0:
            case 4:
            case 5: badspot++; break;
            case 1:
            case 3: okspot++; break;
            case 2: sweetspot++; break;
        }
        switch (Social / 20)
        {
            case 0:
            case 4:
            case 5: badspot++; break;
            case 1:
            case 3: okspot++; break;
            case 2: sweetspot++; break;
        }

        //Debug.Log("Calculating Money:" + badspot + " " + okspot + " " + sweetspot);
        int change = -badspot * 10 + okspot * 5 + sweetspot * 10;
        GlobalVariables.money += change;
        GlobalVariables.currentReputation += change / 10;

        if(change > 0)
        {
            GlobalVariables.guestsHelped++;
        }
        else
        {
            GlobalVariables.guestsLost++;
        }

        return change > 0;
    }

    public void DialoguePop()
    {
        lastDialoguePop = Time.time;
        lastTalked = 0;
    }

}
