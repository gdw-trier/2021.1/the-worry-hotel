using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using DataAccess;
using System.IO;
using System.Globalization;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private GameObject samson;
    [SerializeField] private TextAsset Descriptions;
    [SerializeField] private GameObject[] steps;
    [SerializeField] private TMP_Text[] bubbles;
    private Translation[] descriptions;

    public struct Translation
    {
        public string english { get; set; }
        public string german { get; set; }

        public string getText(GlobalVariables.languages language)
        {
            switch (language)
            {
                case GlobalVariables.languages.English:
                    return english;
                case GlobalVariables.languages.German:
                    return german;
            }
            return string.Empty;
        }
    }

    public void Start ()
    {
        descriptions = loadDialogue(0, descriptions);
        for (int i = 0; i < bubbles.Length; ++i)
        {
            bubbles[i].text = descriptions[i].getText(GlobalVariables.currentLanguage);
        }
    }
    
    public void GoTo2()
    {
        steps[0].SetActive(false);
        steps[1].SetActive(true);
        samson.SetActive(false);
    }

    public void GoTo3()
    {
        steps[1].SetActive(false);
        steps[2].SetActive(true);
    }

    public void GoTo4()
    {
        steps[2].SetActive(false);
        steps[3].SetActive(true);
    }

    public void GoTo5()
    {
        steps[3].SetActive(false);
        steps[4].SetActive(true);
    }

    public void GoTo6()
    {
        steps[4].SetActive(false);
        steps[5].SetActive(true);
    }

    public void GoTo7()
    {
        steps[5].SetActive(false);
        steps[6].SetActive(true);
    }

    public void returnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private Translation[] loadDialogue(int ID, Translation[] array)
    {
        MutableDataTable dt = DataTable.New.ReadFromString(Descriptions.text);
        array = new Translation[dt.NumRows];
        for (int i = 0; i < dt.NumRows; i++)
        {
            Row x = dt.GetRow(i);
            Translation a = x.As<Translation>();
            array[i] = a;
        }
        return array;
    }

    public string getDescritpion(int step)
    {
        return descriptions[step].getText(GlobalVariables.currentLanguage);
    }
}
